#!/usr/bin/env bash

VERSION="${VERSION_MAJOR_MINOR}.${BITBUCKET_BUILD_NUMBER}" 
INFORMATIONAL_VERSION="${VERSION}-${BITBUCKET_COMMIT}"

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

printf "${HIGHLIGHT_YELLOW}Setting azure artifacts credential provider... \n"
export NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED=$NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
export VSS_NUGET_EXTERNAL_FEED_ENDPOINTS=$VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
wget -O - https://raw.githubusercontent.com/Microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh  | bash

printf "${HIGHLIGHT_YELLOW}Building and testing $INFORMATIONAL_VERSION (also includes building nuget package(s))... \n"
dotnet restore -s $DEVOPS_URL -s "https://api.nuget.org/v3/index.json" $SOLUTION_FILEPATH
dotnet build --no-restore --configuration Release -p:VersionPrefix="$VERSION" -p:InformationalVersion="$INFORMATIONAL_VERSION" $SOLUTION_FILEPATH
dotnet test --no-build --configuration Release --filter $DOTNET_TEST_FILTER $SOLUTION_FILEPATH

printf "${HIGHLIGHT_YELLOW}Pushing to azure devops... \n"
for package in $(find **/bin/Release -type f -name *.nupkg); do 
   dotnet nuget push $package -s $DEVOPS_URL -k $DEVOPS_APIKEY
done

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code